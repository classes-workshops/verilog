`timescale 10 ns/1 ns

module simple_logic_tb;
  reg a, b, c;
  wire d;

  simple_logic uut(.a(a), .b(b), .c(c), .d(d));

  initial
  begin
    $dumpfile("/home/lleon95/Documents/Git/Trainings/verilog/simple_logic/simple_logic.vcd");
    $dumpvars(0, a, b, c, d);
    $monitor("simtime = %g, A =%b, B =%b, C =%b D =%b", $time, a, b, c, d);
    a = 1'b0;
    b = 1'b0;
    c = 1'b0;
    #10;
    a = 1'b1;
    #10;
    a = 1'b0;
    b = 1'b1;
    #10;
    b = 1'b0;
    c = 1'b1;
    #10;
    a = 1'b0;
    b = 1'b0;
    #10;
    a = 1'b1;
    #10;
    a = 1'b0;
    b = 1'b1;
    #10;
    b = 1'b0;
    c = 1'b1;
    #10;
    c = 1'b0;
  end
endmodule
