# Verilog Exercises

I am doing some exercises by myself based on my quick scan on the book:

Quick StartGuide to Verilog by Brock J. LaMeres

Some tools required:

* Icarus Verilog
* GTKWave
* TerosHDL (for Visual Studio)
