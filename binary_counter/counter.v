module counter(
    input wire clk, rst,
    output wire[3:0] out);

  reg[3:0] cnt = 4'd0;

  always @ (posedge clk)
  begin
    if (rst == 1'b1)
      cnt <= 4'd0;
    else
      cnt <= cnt + 4'd1;
  end

  assign out = cnt;
endmodule
