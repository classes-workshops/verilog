`timescale 10 ns/1 ns

module counter_tb;
  reg clk = 0, rst;
  wire [3:0] out;

  counter cnt1(.clk(clk), .rst(rst), .out(out));

  // Test logic
  initial 
  begin
    $dumpfile("/home/lleon95/Documents/Git/Trainings/verilog/binary_counter/counter.vcd");
    $dumpvars(0, clk, rst, out);
    $monitor("simtime = %g, Rst =%b, Out =%d", $time, rst, out);
    rst = 1;
    #10;
    rst = 0;
    #500; $display("Finished"); $stop;
  end

  // Clock generator
  always begin
    #10; clk = ~clk;
  end
endmodule
